/**
 * Interaction Mediator Component for askme!
 *
 * @author Felix Dürrwald (mplabs@mplabs.de)
 * @since 2013-05-02
 * @version 0.1
 */

/**
 * Global sugar
 */

// This does cross-browser event binding
if ( typeof Object.prototype.on === 'undefined' ) {
	Object.prototype.on = function (type, cb) {
		var that = this;

		if ( this.addEventListener ) {
			this.addEventListener(type, cb, false);
		} else if ( this.attachEvent ) {
			this.attachEvent('on' + type, function () {
				return fb.call(that, window.event);
			});
		}
	};
}

// This does cross-browser event triggering
if ( typeof Object.prototype.trigger === 'undefined' ) {
	Object.prototype.trigger = function (type, data) {
		var evt = new CustomEvent(type, { detail: data});

		if ( this.fireEvent ) {
			this.fireEvent('on' + type, evt);
		} else {
			this.dispatchEvent(evt);
		}
	}
}

/**
 * SIDAC interaction mediator
 *
 * Exports:
 *   - getSetup()
 *
 */
 var SIDAC_IM = (function ($) {
 	// Private attributes
 	var _options = {}
 	  , _stack = []
 	  , _state = {};

	/**
	 * Class constructor
	 * 
	 * @param Array options
	 * @return Object
	 */
	var SIDAC_IM = function (options) {
		_options = options || {};

		// Setup event Handlers
		window.on('exception', _handleException);
	};

	// Public methods

	/**
	 * set state
	 * 
	 * @param string|object key or map of values
	 * @param mixed|void value
	 * @return mixed returns set data
	 */
	SIDAC_IM.prototype.set = function () {
		if ( typeof arguments[0] !== 'string' ) {
			for ( var key in arguments[0] ) {
				this.set(key, arguments[0][key]);
			}
		}

		_state[arguments[0]] = arguments[1];

		return this;
	};

	/**
	 * get state
	 * 
	 * @param string the option to get
	 * @return mixed
	 */
	SIDAC_IM.prototype.get = function (key) {
		if ( typeof key === 'undefined' ) {
			throw "Cannot get undefined key from state.";
		}

		if ( ! _state[key] ) {
			throw "Unknown property " + key;
		}

		return _state[key];
	};

	/**
	 * Initialize the component
	 *
	 * @return String
	 */
	SIDAC_IM.prototype.init = function (options, cb) {
		var data = {};

		if ( typeof options === 'object' ) {
			this.set(options);
		}

		cb.call(this, data);

		return this;
	};

	SIDAC_IM.prototype.on = function (key, cb, once) {
		once = once || false;

		_stack.push({
			'event': key,
			'callback': cb,
			'once': once
		});

		return this;
	};

	SIDAC_IM.prototype.one = function (key, cb) {
		return this.one(key, cb, true);
	};

	SIDAC_IM.prototype.off = function (key) {
		if ( _stack[key] ) {
			delete _stack[key];	
		}
		
		return this;
	}

	// Private methods

<div></div>	/**
	 * Handle exception from the SIDAC component
	 * 
	 * @param Event
	 * @return void
	 */
	function _handleException (e) {
		var data = e.detail;

		if ( ! data.name ) {
			throw "Received unspezified event";
		}

		if ( _stack[data.name] ) {
			_stack[data.name].callback.apply(this, data, e);
		}
	};

	// Export
	return SIDAC_IM;
	
})(jQuery);