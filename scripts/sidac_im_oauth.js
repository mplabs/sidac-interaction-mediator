/**
 * Interaction Mediator OAuth consumer
 *
 * @author Felix Dürrwald (mplabs@mplabs.de)
 * @since 2013-05-05
 * @version 0.1
 */
(function (SIDAC_IM) {

	// Private attributes

	var consumerKey = ''
	  , consumerSecret = ''
	  , oauthSignature = ohauth.signature({
		    oauth_consumer_key: '...'
		  , oauth_signature_method: '...'
		  , oauth_timestamp: '...'
		  , oauth_nonce: '...'
		})
	  , oAuthProvider = {
		  	signatureMethod: 'HMAC-SHA1'
		  , requestTokenUrl: 'https://askme.idmt.fraunhofer.de/request_token'
		  , userAuthorizationUrl: 'https://askme.idmt.fraunhofer.de/authorize'
		  , accessTokenUrl: 'https://askme.idmt.fraunhofer.de/access_token'
		  , echoUrl: 'https://askme.idmt.fraunhofer.de/echo'
	 	}
	  ;

	SIDAC_IM.prototype.getToken = function () {
		
	};

})(SIDAC_IM);